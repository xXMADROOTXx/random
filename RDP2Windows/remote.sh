#!/bin/bash

# DATE 15.06.2018
# Authors :
# - Emad Almestadi "almestae@gmail.com"
# - Majed Alharbi "eng.mjdharbi@gmail.com"
# Description: run the script to connect RDP to our Windows lab server.

# Ask for the IP address as input
read -p "Enter you server IP Address: " IP

# Ask for the username to feed it to xfreerdp.
read -p "Enter username: " USERNAME

# Print screen size to add it as option in xfreerdp.
echo "[1] 1920x1080	[2] 1024x768	[3] 800x600"

# read user input based on the three options above.
read -p "Chose Screen Size: " SIZE


# Take input and feed it to g varialbe for xfreerdp

case $SIZE in
1)
	g='1920x1080'
	echo $g
	;;
2)
	g='1024x768'
	echo $g
	;;
3)
	g='800x600'
	echo $g
	;;
*)
	echo "invalid input: Error"
	;;
esac

xfreerdp -u $USERNAME -g $g $IP


