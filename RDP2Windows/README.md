## Welcome to this Simple Script 
I wrote this script to help me connect to a windows machine using [*xfreerdp*](http://www.freerdp.com/). 

### Usage
Simply download the script, and make sure that you have xfreerdp installed. 

- Ubuntu
``` 
 # sudo apt install lightdm-remote-session-freerdp2
 # bash remote.sh 
```
It will prompt you with questions and options. Just hit the desired number then you will have your RDP session.